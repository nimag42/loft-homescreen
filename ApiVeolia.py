import requests
import datetime
import json

class ApiVeolia:
    def __init__(self, idContract, idMeter, idPDS):
        self.idContract = idContract
        self.idMeter = idMeter
        self.idPDS = idPDS

    def get_water(self, date=None):
        api_url = 'https://espace-client.vedif.eau.veolia.fr/s/sfsites/aura'

        if(date == None):
            date = datetime.date.today() + datetime.timedelta(1)
            date_end = date.strftime("%Y-%m-%d")

        params = (
            ('other.LTN015_ICL_ContratConsoHisto.getData', '1'),
        )

        data = {
            'message': '{"actions":[{"id":"756;a","descriptor":"apex://LTN015_ICL_ContratConsoHisto/ACTION$getData","callingDescriptor":"markup://c:ICL_Contrat_Conso_Historique","params":{"contractId":"' + self.idContract +'","TYPE_PAS":"JOURNEE","DATE_DEBUT":"2019-03-01","DATE_FIN":"' + date_end + '","NUMERO_COMPTEUR":"' + self.idMeter + '","ID_PDS":"' + self.idPDS+ '"},"version":null}]}',
            'aura.context': '{"mode":"PROD","fwuid":"pxtF0ZdGYF-z00XsmRUcGQ","app":"siteforce:communityApp","loaded":{"APPLICATION@markup://siteforce:communityApp":"ZuzLa88WrV7zCTrNDj3zHA"},"dn":[],"globals":{},"uad":false}',
            'aura.pageURI': '/s/contrats?tab=Detail',
            'aura.token': '' # Magically, it seems veolia doesn't check token.
        }
        cookies = {}

        response = requests.post(api_url, params=params, data=data)
        raw = json.loads(response.text)
        conso = raw['actions'][0]['returnValue']['data']['CONSOMMATION']
        return conso
