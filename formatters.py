import datetime

def na_weather(raw_measures, raw_types):
    if raw_measures.get('status', 'err') != 'ok' or len(raw_measures.get('body', [])) == 0:
        print ('Error malformated netatmo\'s message', raw_measures)
        return False

    # Sort results by time
    sorted_measures = sorted(raw_measures['body'], key=lambda k: k.get('beg_time', 0), reverse=True)
    measures = sorted_measures[0]['value'][-1]
    types = raw_types.split(',')

    return {
        types[i]: measures[i] for i in range(len(types))
    }

def OWMweatherIdToConst(wid):
    if (wid >= 200 and wid < 300):
        return 'orage'
    elif (wid == 300 or wid == 310):
        return 'crachin léger'
    elif (wid == 301 or wid == 311):
        return 'crachin'
    elif (wid >= 302 and wid < 400):
        return 'crachin intense'
    elif (wid == 500 or wid == 520):
        return 'pluie légère'
    elif (wid == 501 or wid == 521):
        return 'pluie'
    elif (wid >= 502 and wid < 600):
        return 'pluie intense'
    elif (wid == 600 or wid == 620):
        return 'neige légère'
    elif (wid == 601 or wid == 621):
        return 'neige'
    elif (wid >= 502 and wid < 600):
        return 'neige intense'
    elif (wid == 701):
        return 'brume'
    elif (wid == 721):
        return 'brume épaisse'
    elif (wid == 741):
        return 'brouillard'
    elif (wid == 800):
        return 'ciel clair'
    elif (wid == 801):
        return 'nuages épars'
    elif (wid == 802):
        return 'nuageux'
    elif (wid == 803 or wid == 804):
        return 'entièrement nuageux'
    return 'NA'

def owm_weather(raw_measure):

    return {
        'temp': raw_measure.get('main', {}).get('temp', float('nan')),
        'humidity': raw_measure.get('main', {}).get('humidity', float('nan')),
        'weather': OWMweatherIdToConst(raw_measure.get('weather', [{}])[0].get('id', 0)),
        'wind': raw_measure.get('wind', {}),
        'cloud_cover': raw_measure.get('clouds', {}).get('all', float('nan')),
    }

def owm_forecast(raw_measure):
    measures = [
        {
            'ts': measure.get('dt', 0),
            'cloud_cover': measure.get('clouds', {}).get('all', float('nan')),
            'temp': measure.get('main', {}).get('temp', float('nan')),
            'humidity': measure.get('main', {}).get('humidity', float('nan')),
            'weather': OWMweatherIdToConst(measure.get('weather', [{}])[0].get('id', 0)),
            'wind': measure.get('wind'),
            'rain': measure.get('rain')
        } for measure in raw_measure.get('list', [])
    ]

    # UTC, +1 for CET
    matin = 4
    aprem = 11
    soir = 16
    nuit = 23

    begin = datetime.datetime.now().replace(minute=0, second=0, microsecond=0)
    end = datetime.datetime.now().replace(minute=0, second=0, microsecond=0)
    hour = datetime.datetime.now().hour
    if hour < matin:
        begin = begin.replace(hour=matin)
        end = end.replace(hour=aprem)
    elif hour < aprem:
        begin = begin.replace(hour=aprem)
        end = end.replace(hour=soir)
    elif hour < soir:
        begin = begin.replace(hour=soir)
        end = end.replace(hour=nuit)
    elif hour < nuit:
        begin = begin.replace(hour=nuit)
        end = end.replace(hour=matin) + datetime.timedelta(days=1)
    else:
        begin = begin.replace(hour=matin) + datetime.timedelta(days=1)
        end = end.replace(hour=aprem) + datetime.timedelta(days=1)

    filtered_measures = []
    for measure in measures:
        if begin.timestamp() <= measure['ts'] <= end.timestamp():
            filtered_measures.append(measure)

    return filtered_measures

