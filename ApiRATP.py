import datetime

from xml.etree import ElementTree

import requests

def requestNextMission(station_id, line_id, way):
    host = "http://opendata-tr.ratp.fr/wsiv/services/Wsiv"
    headers = {
        'content-type': 'text/soap+xml',
        'charset': 'utf-8',
        'SOAPAction': 'urn:getMissionsNext'
    }
    request_tpl = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://wsiv.ratp.fr/xsd" xmlns:wsiv="http://wsiv.ratp.fr"><soapenv:Header/><soapenv:Body><wsiv:getMissionsNext><wsiv:station><xsd:id>{}</xsd:id><xsd:line><xsd:id>{}</xsd:id></xsd:line></wsiv:station><wsiv:direction><xsd:sens>{}</xsd:sens></wsiv:direction></wsiv:getMissionsNext></soapenv:Body></soapenv:Envelope>'
    request = request_tpl.format(station_id, line_id, way)

    raw = requests.post(host, data=request, headers=headers)
    response = ElementTree.fromstring(raw.text)[0][0][0]
    nextMissions = []

    for child in response:
        if child.tag.endswith('missions'):
            mission = {'date': None, 'message': ''}
            for missionChild in child:
                if missionChild.tag.endswith('stationsDates'):
                    mission['date'] = datetime.datetime.strptime(missionChild.text, "%Y%m%d%H%M")
                elif missionChild.tag.endswith('stationsMessages'):
                    mission['message'] = missionChild.text
                    nextMissions.append(mission)

    return nextMissions
