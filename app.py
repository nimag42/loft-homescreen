import time
import subprocess
from flask import Flask, jsonify, render_template

import config

import formatters
import ApiRATP
from ApiNetatmo import ApiNetatmo
from ApiOWM import ApiOWM
from ApiVeolia import ApiVeolia

app = Flask(__name__)

api_na = ApiNetatmo(config.netatmo_app_id, config.netatmo_app_secret, config.netatmo_refresh_token, config.netatmo_username)
api_owm = ApiOWM(config.owm_app_id)
api_veolia = ApiVeolia(config.veolia_contract_id, config.veolia_id_meter, config.veolia_id_PDS)

api_version = 1#subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).strip().decode()

@app.route('/')
def index():
    return render_template(
        'index.html',
        api_version = api_version,
        missions = str(list(config.missions.keys()))
    )

@app.route('/ratp')
def ratp():
    if app.debug:
        return jsonify({
            key: [{"date":"Sun, 25 Nov 2018 19:29:00 GMT","message":"7 mn"},{"date":"Sun, 25 Nov 2018 19:44:00 GMT","message":"22 mn"}] for (key,val) in config.missions.items()
        })
    else:
        return jsonify({
            key: ApiRATP.requestNextMission(*val) for (key,val) in config.missions.items()
        })

@app.route('/water_consumption')
def water_consumption():
    raw = api_veolia.get_water()
    historic_water_consumption = [float(c['CONSOMMATION'])*1000 for c in raw]
    historic_water_consumption_date = [c['DATE_INDEX'] for c in raw]

    return jsonify({
        'x': historic_water_consumption_date,
        'y': historic_water_consumption,
    })

@app.route('/weather')
def weather():
    current_na_weather = formatters.na_weather(
        api_na.get_measures(
            time.time() - 20*60,
            time.time(),
            config.device,
            config.module_outdoor,
            'temperature,humidity'
        ),
        'temperature,humidity'
    )

    current_owm_weather = formatters.owm_weather(
        api_owm.get_weather(config.owm_city_id)
    )

    forecast_owm = formatters.owm_forecast(
        api_owm.get_forecast(config.owm_city_id, 4)
    )

    return jsonify({
        'current': current_na_weather,
        'owm': current_owm_weather,
        'forecast': forecast_owm,
    })

@app.after_request
def add_api_version(response):
    response.headers["API-VERSION"] = api_version
    return response
