import requests
import getpass

class ApiNetatmo:
    api_url = "https://api.netatmo.com/"

    def __init__(self, client_id, client_secret, refresh_token, username):
        self.client_id = client_id
        self.client_secret = client_secret
        self.refresh_token = refresh_token
        self.username = username

        self._refresh_token()

    def _refresh_token(self):
        payload = {
            'grant_type': 'refresh_token',
            'refresh_token': self.refresh_token,
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'scope': 'read_station'
        }

        try:
            response = requests.post(self.api_url + "oauth2/token", data=payload)

            self.token = response.json()["access_token"]
            self.refresh_token = response.json()["refresh_token"]

        except Exception as e:
            raise e

    def auth_with_password(self):
        payload = {
            'grant_type': 'password',
            'username': self.username,
            'password': "",
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'scope': 'read_station'
        }

        payload['password'] = getpass.getpass('Password : ')

        try:
            response = requests.post(self.api_url + "oauth2/token", data=payload)

            print(response.json())
            token = response.json()["access_token"]
            refresh_token = response.json()["refresh_token"]
            scope = response.json()["scope"]

        except Exception as e:
            print(e)

    def get_last_measure(self, mac, retry=True):
        print(mac)
        payload = {
            'access_token': self.token,
            'device_id': mac,
        }

        try:
            response = requests.post(self.api_url + "api/getpublicmeasure", data=payload)

            if 'error' in response.json() and response.json()['error']['code'] in [2, 3] and retry:
                self._refresh_token()
                return self.get_last_measure(mac, False)
            elif 'error' in response.json():
                print('Error in netatmo\'s response', response.json())

            return response.json()
        except Exception as e:
            print(e)

    def get_measures(self, begin, end, device_mac, module_out_mac, typeM, retry=True):
        payload = {
            'access_token': self.token,
            'date_begin': begin,
            'date_end': end,
            'device_id': device_mac,
            'module_id': module_out_mac,
            'scale': 'max',
            'type': typeM
        }

        try:
            response = requests.post(self.api_url + "api/getmeasure", data=payload)
            if 'error' in response.json() and response.json()['error']['code'] in [2, 3] and retry:
                self._refresh_token()
                return self.get_measures(
                    begin, end, device_mac, module_out_mac, typeM, False
                )
            elif 'error' in response.json():
                print('Error in netatmo\'s response', response.json())

            return response.json()
        except Exception as e:
            print(e)
