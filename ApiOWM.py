import requests

class ApiOWM:
    api_url = "https://api.openweathermap.org/data/2.5/"

    def __init__(self, app_id):
        self.app_id = app_id


    def get_weather(self, city_id):
        req_url = '?id=' + city_id + '&units=metric&APPID=' + self.app_id

        try:
            response = requests.post(self.api_url + "weather/" + req_url)
            return response.json()
        except Exception as e:
            print(e)

    def get_forecast(self, city_id, count):
        req_url = '?id=' + city_id + '&cnt=' + str(count) + '&units=metric&APPID=' + self.app_id

        try:
            response = requests.post(self.api_url + "forecast/" + req_url)
            return response.json()
        except Exception as e:
            print(e)

