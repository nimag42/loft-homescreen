# Netatmo API
netatmo_app_id = 'Netatmo APP ID'
netatmo_app_secret = 'Netatmo APP secret'
netatmo_refresh_token = 'Netatmo refresh token for Oauth2'
netatmo_username = 'Your username'
device = 'mac of device'
module_outdoor = 'mac of outdoor module'

# RATP Api
missions = {
    'bus line number': ['id bus', 'id number', 'sens'],
    #'194': ['194_7614_7615', 'B194', 'R']
}

# OpenWeatherMap API
owm_app_id = "OWM APP ID"
owm_city_id = "City ID"

# Veolia ids
veolia_contract_id = ""
veolia_id_meter = ""
veolia_id_PDS = ""